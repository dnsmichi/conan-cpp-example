return()
set(_conan_cmake_binary_path "${CMAKE_BINARY_DIR}/conan.cmake")
if(NOT EXISTS "${_conan_cmake_binary_path}")
    message(STATUS "Downloading conan.cmake from https://raw.githubusercontent.com/conan-io/cmake-conan/v0.15/conan.cmake")
    file(DOWNLOAD "https://raw.githubusercontent.com/conan-io/cmake-conan/v0.15/conan.cmake"
                    "${_conan_cmake_binary_path}"
         STATUS __status)
    list(GET __status 0 __status_code)
    list(GET __status 1 __status_string)
    if(NOT "${__status_code}" EQUAL 0)
        if(EXISTS "${_conan_cmake_binary_path}")
            file(REMOVE "${_conan_cmake_binary_path}") # remove invalid file
        endif()
        message(FATAL_ERROR "Download failed => code: ${__status_code}, error: ${__status_string}")
    else()
        message(STATUS "Download succeeded")
    endif()
endif()

if(NOT EXISTS "${_conan_cmake_binary_path}")
    message(FATAL_ERROR "Could not find conan.cmake, maybe download failed => check status")
endif()
include("${_conan_cmake_binary_path}")

conan_cmake_run(CONANFILE conanfile.py
                BASIC_SETUP CMAKE_TARGETS
                BUILD_TYPE ${CMAKE_BUILD_TYPE}
                )

set(Qt5_DIR "${CMAKE_BINARY_DIR}/_qt/lib/cmake/Qt5")
if(NOT EXISTS "${Qt5_DIR}")
    message(FATAL_ERROR "Qt5_DIR '${Qt5_DIR}' does not exists")
endif()

# search with qt find_package to get support for moc, uic and res
find_package(Qt5
             COMPONENTS
             Core
             Widgets
             REQUIRED)
