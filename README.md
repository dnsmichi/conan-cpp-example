# Conan features on GitLab.com
This repo demonstrate [conan](https://conan.io) features on GitLab.com including

* [GitLab Conan Registry](https://docs.gitlab.com/ee/user/packages/conan_repository)
* [Windows Shared Runner](https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/gcp/windows-containers/blob/master/cookbooks/preinstalled-software/README.md)
