#ifndef DOG_H
#define DOG_H

#include "mylib_module.h"

class MYLIB_EXPORT Dog
{
public:
    Dog();
    void sound();
};

#endif
