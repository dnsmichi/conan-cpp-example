#ifndef MYLIB_MODULE_H
#define MYLIB_MODULE_H

#include <QtCore/qglobal.h>

#ifdef MYLIB_MODULE_LIB
# define MYLIB_EXPORT Q_DECL_EXPORT
#else
# define MYLIB_EXPORT Q_DECL_IMPORT
#endif

#endif
