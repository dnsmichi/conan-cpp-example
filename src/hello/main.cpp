#include <QApplication>
#include <QLabel>
#include <iostream>
#include <boost/any.hpp>
#include <vector>
#include <string>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    std::cout << "hello :-D\n";

    boost::any variable(std::string("Hello world!"));
    std::string s1 = boost::any_cast<std::string>(variable);
    
    std::cout << "any is " << s1;

    QLabel label("hello");
    label.show();

    return app.exec();
}