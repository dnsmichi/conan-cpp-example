import os
from conans import ConanFile, CMake, tools


class ExampleConan(ConanFile):
    name = "example"
    version = "1.0.0"
    license = "<Put the package license here>"
    author = "<Put your name here> <And your email here>"
    url = "<Package recipe repository url here, for issues about the package>"
    description = "<Description of Example here>"
    topics = ("<Put some tag here>", "<here>", "<and here>")
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = {"shared": True}
    generators = "cmake"
    #requires = ("qt/5.12.9@bincrafters/stable",
    #            "boost/1.73.0"
    #            )
    scm = {
        "type": "git",
        "url": "https://gitlab.com/tonka3000/conan-cpp-example.git",
        "revision": "auto"
    }
    no_copy_source = True
    _cmake = None

    def imports(self):
        for pkg, _ in self.deps_cpp_info.dependencies:
            if not pkg.endswith("_installer"):
                self.copy(pattern="*.dll", src="bin", dst="bin", root_package=pkg)
                self.copy(pattern="*.so*", src="lib", dst="lib", root_package=pkg, keep_path=True)
            if pkg.startswith("qt"):
                for pattern in ["*.dll", "*.so*"]:
                    self.copy(pattern=pattern, src="plugins", dst="plugins", root_package=pkg, keep_path=True)
                    for plugin_folder in ["platforms", "sqldrivers"]:
                        self.copy(pattern=pattern,
                                  src="plugins/%s" % plugin_folder,
                                  dst="bin/%s" % plugin_folder, root_package=pkg,
                                  keep_path=True
                                  )
                if self.settings.compiler != "Visual Studio":
                    self.copy(pattern="*", src="lib/fonts", dst="bin/lib/fonts", root_package=pkg, keep_path=True)
            if pkg.startswith("qt"):
                self.copy(pattern="*", src="", dst="_qt", root_package=pkg, keep_path=True)

    def _configure_cmake(self):
        if not self._cmake:
            self._cmake = CMake(self)
            defs = {
                "CMAKE_BUILD_TYPE" : self.settings.build_type
            }
            self._cmake.configure(defs=defs)
        return self._cmake

    def build(self):
        cmake = self._configure_cmake()
        cmake.build()

    def package(self):
        cmake = self._configure_cmake()
        cmake.install()

    def package_info(self):
        #self.cpp_info.libs = ["hello"]
        self.env_info.PATH.append(os.path.join(self.package_folder, "bin"))

